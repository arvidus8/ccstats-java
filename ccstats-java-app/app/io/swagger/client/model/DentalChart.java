/*
 * Cloud Clinic API
 * API for Cloud Clinic
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.DentalChartForPosting;
import io.swagger.client.model.Tooth;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;

/**
 * DentalChart
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-03-28T10:16:13.373+02:00")
public class DentalChart {
  @SerializedName("created_by")
  private String createdBy = null;

  /**
   * Dentition for the dental chart
   */
  @JsonAdapter(DentitionEnum.Adapter.class)
  public enum DentitionEnum {
    PERMANENT("permanent"),
    
    PRIMARY("primary"),
    
    MIXED("mixed");

    private String value;

    DentitionEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static DentitionEnum fromValue(String text) {
      for (DentitionEnum b : DentitionEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    public static class Adapter extends TypeAdapter<DentitionEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final DentitionEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public DentitionEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return DentitionEnum.fromValue(String.valueOf(value));
      }
    }
  }

  @SerializedName("dentition")
  private DentitionEnum dentition = null;

  @SerializedName("teeth")
  private List<Tooth> teeth = new ArrayList<Tooth>();

  @SerializedName("title")
  private String title = null;

  @SerializedName("created_ts")
  private OffsetDateTime createdTs = null;

  @SerializedName("id")
  private Integer id = null;

  @SerializedName("last_modified_ts")
  private OffsetDateTime lastModifiedTs = null;

  public DentalChart createdBy(String createdBy) {
    this.createdBy = createdBy;
    return this;
  }

   /**
   * The id of the user who created the dental chart
   * @return createdBy
  **/
  @ApiModelProperty(value = "The id of the user who created the dental chart")
  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public DentalChart dentition(DentitionEnum dentition) {
    this.dentition = dentition;
    return this;
  }

   /**
   * Dentition for the dental chart
   * @return dentition
  **/
  @ApiModelProperty(example = "permanent", required = true, value = "Dentition for the dental chart")
  public DentitionEnum getDentition() {
    return dentition;
  }

  public void setDentition(DentitionEnum dentition) {
    this.dentition = dentition;
  }

  public DentalChart teeth(List<Tooth> teeth) {
    this.teeth = teeth;
    return this;
  }

  public DentalChart addTeethItem(Tooth teethItem) {
    this.teeth.add(teethItem);
    return this;
  }

   /**
   * List of teeth belonging to this dental chart
   * @return teeth
  **/
  @ApiModelProperty(required = true, value = "List of teeth belonging to this dental chart")
  public List<Tooth> getTeeth() {
    return teeth;
  }

  public void setTeeth(List<Tooth> teeth) {
    this.teeth = teeth;
  }

  public DentalChart title(String title) {
    this.title = title;
    return this;
  }

   /**
   * The title of the dental chart
   * @return title
  **/
  @ApiModelProperty(value = "The title of the dental chart")
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public DentalChart createdTs(OffsetDateTime createdTs) {
    this.createdTs = createdTs;
    return this;
  }

   /**
   * The time a dental chart was created
   * @return createdTs
  **/
  @ApiModelProperty(value = "The time a dental chart was created")
  public OffsetDateTime getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(OffsetDateTime createdTs) {
    this.createdTs = createdTs;
  }

  public DentalChart id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * The unique identifier of a dental chart
   * @return id
  **/
  @ApiModelProperty(value = "The unique identifier of a dental chart")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DentalChart lastModifiedTs(OffsetDateTime lastModifiedTs) {
    this.lastModifiedTs = lastModifiedTs;
    return this;
  }

   /**
   * The last time a dental chart was modified
   * @return lastModifiedTs
  **/
  @ApiModelProperty(value = "The last time a dental chart was modified")
  public OffsetDateTime getLastModifiedTs() {
    return lastModifiedTs;
  }

  public void setLastModifiedTs(OffsetDateTime lastModifiedTs) {
    this.lastModifiedTs = lastModifiedTs;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DentalChart dentalChart = (DentalChart) o;
    return Objects.equals(this.createdBy, dentalChart.createdBy) &&
        Objects.equals(this.dentition, dentalChart.dentition) &&
        Objects.equals(this.teeth, dentalChart.teeth) &&
        Objects.equals(this.title, dentalChart.title) &&
        Objects.equals(this.createdTs, dentalChart.createdTs) &&
        Objects.equals(this.id, dentalChart.id) &&
        Objects.equals(this.lastModifiedTs, dentalChart.lastModifiedTs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(createdBy, dentition, teeth, title, createdTs, id, lastModifiedTs);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DentalChart {\n");
    
    sb.append("    createdBy: ").append(toIndentedString(createdBy)).append("\n");
    sb.append("    dentition: ").append(toIndentedString(dentition)).append("\n");
    sb.append("    teeth: ").append(toIndentedString(teeth)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    createdTs: ").append(toIndentedString(createdTs)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    lastModifiedTs: ").append(toIndentedString(lastModifiedTs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}


package controllers;

import io.swagger.client.ApiException;
import io.swagger.client.api.ClinicsApi;
import io.swagger.client.api.PatientsApi;
import io.swagger.client.model.*;
import play.mvc.Controller;

import views.html.home.*;

import java.util.ArrayList;
import java.util.List;

public class HomeController extends Controller {

    private static ClinicsApi clinics = new ClinicsApi();
    private static PatientsApi patients = new PatientsApi();

    public static List<ClinicRead> getAllClinics(String token) throws ApiException {

        System.out.println("Din token: " + token);

        List<ClinicRead> clinicList = clinics.getClinicsCollection(token);

        return clinicList;
    }

    public static List<ClinicRead> getClinicRoles(String token, String id) throws ApiException {

        List<ClinicRead> clinicList = getAllClinics(token);
        List<ClinicRead>  userClinics = new ArrayList<>();
        List<UserAndRole> tempClinics = new ArrayList<>();

        for (ClinicRead clinic : clinicList){
            tempClinics = clinics.getClinicItemWithRolesCollection(token, clinic.getId());
            for (UserAndRole roles : tempClinics){
                if (roles.getUserId().equals(id)){
                    userClinics.add(clinic);
                }
            }
        }

        return userClinics;
    }

    public static List<PatientRead> getPatients(String token, String clinicId) throws ApiException {

        List<PatientRead> patientList = patients.getPatientsCollection(token, clinicId);

        return patientList;
    }
}

/*
 * Cloud Clinic API
 * API for Cloud Clinic
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.User;
import java.io.IOException;

/**
 * UserCreate
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-03-28T10:16:13.373+02:00")
public class UserCreate {
  @SerializedName("email")
  private String email = null;

  @SerializedName("first_name")
  private String firstName = null;

  @SerializedName("is_confirmed")
  private Boolean isConfirmed = false;

  @SerializedName("last_name")
  private String lastName = null;

  @SerializedName("middle_name")
  private String middleName = null;

  @SerializedName("office_phone_number")
  private String officePhoneNumber = null;

  @SerializedName("phone_number")
  private String phoneNumber = null;

  /**
   * Status of the user
   */
  @JsonAdapter(StatusEnum.Adapter.class)
  public enum StatusEnum {
    ACTIVE("active"),
    
    INACTIVE("inactive"),
    
    DELETED("deleted");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static StatusEnum fromValue(String text) {
      for (StatusEnum b : StatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    public static class Adapter extends TypeAdapter<StatusEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final StatusEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public StatusEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return StatusEnum.fromValue(String.valueOf(value));
      }
    }
  }

  @SerializedName("status")
  private StatusEnum status = null;

  @SerializedName("password")
  private String password = null;

  public UserCreate email(String email) {
    this.email = email;
    return this;
  }

   /**
   * The email used for authentication
   * @return email
  **/
  @ApiModelProperty(required = true, value = "The email used for authentication")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public UserCreate firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

   /**
   * First name
   * @return firstName
  **/
  @ApiModelProperty(value = "First name")
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public UserCreate isConfirmed(Boolean isConfirmed) {
    this.isConfirmed = isConfirmed;
    return this;
  }

   /**
   * Email is verified
   * @return isConfirmed
  **/
  @ApiModelProperty(value = "Email is verified")
  public Boolean isIsConfirmed() {
    return isConfirmed;
  }

  public void setIsConfirmed(Boolean isConfirmed) {
    this.isConfirmed = isConfirmed;
  }

  public UserCreate lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

   /**
   * Last name
   * @return lastName
  **/
  @ApiModelProperty(value = "Last name")
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public UserCreate middleName(String middleName) {
    this.middleName = middleName;
    return this;
  }

   /**
   * Middle name
   * @return middleName
  **/
  @ApiModelProperty(value = "Middle name")
  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public UserCreate officePhoneNumber(String officePhoneNumber) {
    this.officePhoneNumber = officePhoneNumber;
    return this;
  }

   /**
   * Office Phone number
   * @return officePhoneNumber
  **/
  @ApiModelProperty(value = "Office Phone number")
  public String getOfficePhoneNumber() {
    return officePhoneNumber;
  }

  public void setOfficePhoneNumber(String officePhoneNumber) {
    this.officePhoneNumber = officePhoneNumber;
  }

  public UserCreate phoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }

   /**
   * Phone number
   * @return phoneNumber
  **/
  @ApiModelProperty(value = "Phone number")
  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public UserCreate status(StatusEnum status) {
    this.status = status;
    return this;
  }

   /**
   * Status of the user
   * @return status
  **/
  @ApiModelProperty(example = "active", value = "Status of the user")
  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public UserCreate password(String password) {
    this.password = password;
    return this;
  }

   /**
   * The password used for authentication
   * @return password
  **/
  @ApiModelProperty(value = "The password used for authentication")
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserCreate userCreate = (UserCreate) o;
    return Objects.equals(this.email, userCreate.email) &&
        Objects.equals(this.firstName, userCreate.firstName) &&
        Objects.equals(this.isConfirmed, userCreate.isConfirmed) &&
        Objects.equals(this.lastName, userCreate.lastName) &&
        Objects.equals(this.middleName, userCreate.middleName) &&
        Objects.equals(this.officePhoneNumber, userCreate.officePhoneNumber) &&
        Objects.equals(this.phoneNumber, userCreate.phoneNumber) &&
        Objects.equals(this.status, userCreate.status) &&
        Objects.equals(this.password, userCreate.password);
  }

  @Override
  public int hashCode() {
    return Objects.hash(email, firstName, isConfirmed, lastName, middleName, officePhoneNumber, phoneNumber, status, password);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserCreate {\n");
    
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    isConfirmed: ").append(toIndentedString(isConfirmed)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    middleName: ").append(toIndentedString(middleName)).append("\n");
    sb.append("    officePhoneNumber: ").append(toIndentedString(officePhoneNumber)).append("\n");
    sb.append("    phoneNumber: ").append(toIndentedString(phoneNumber)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}


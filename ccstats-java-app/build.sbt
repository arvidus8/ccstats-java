name := "ccstats-java-app"
 
version := "1.0" 
      
lazy val `ccstats-java-app` = (project in file(".")).enablePlugins(PlayJava)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
scalaVersion := "2.11.11"

libraryDependencies ++= Seq( javaJdbc , cache , javaWs, "io.swagger" % "swagger-annotations" % "1.5.15",
  "com.squareup.okhttp" % "okhttp" % "2.7.5",
  "com.squareup.okhttp" % "logging-interceptor" % "2.7.5",
  "com.google.code.gson" % "gson" % "2.8.1",
  "org.threeten" % "threetenbp" % "1.3.5" % "compile",
  "io.gsonfire" % "gson-fire" % "1.8.0" % "compile",
  "junit" % "junit" % "4.12" % "test",
  "com.novocode" % "junit-interface" % "0.10" % "test" )

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

      
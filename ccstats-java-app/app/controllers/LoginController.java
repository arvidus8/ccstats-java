package controllers;

import com.google.common.net.HttpHeaders;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.Configuration;
import io.swagger.client.api.LoginApi;
import io.swagger.client.model.Login;

import io.swagger.client.model.Token;
import org.apache.commons.codec.binary.Hex;

import play.mvc.Controller;
import play.mvc.Result;
import play.data.DynamicForm;
import play.data.Form;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import views.html.*;
import views.html.home.*;
import views.html.home.home;

import static play.mvc.Results.ok;

public class LoginController extends Controller {

    private Configuration config = new Configuration();
    public LoginApi client = new LoginApi();


    public Result onSubmit() {

        Login payload = new Login();
        Token token = new Token();
        DynamicForm dynamicForm = Form.form().bindFromRequest();


        payload.setEmail(dynamicForm.get("username"));
        System.out.println("This is the email: " + payload.getEmail());

        try {
            MessageDigest mda = MessageDigest.getInstance("SHA-512");
            byte [] digestPass = mda.digest(dynamicForm.get("password").getBytes());
            char[] byteArray = Hex.encodeHex(digestPass);
            String bytes = "";
            for (char c : byteArray){
                bytes += c;
            }
            payload.setPassword(bytes);
            System.out.println("This is the Password hash: " + payload.getPassword());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        System.out.println(payload.toString());

//        payload.setPassword(dynamicForm.get("password"));

        try {
            token = client.postAuthentication(payload);
        } catch (ApiException e) {
            e.printStackTrace();
        }

        System.out.println(token);

        return ok(home.render(token.getId(), "Bearer " + token.getAccessToken()));
    }
}

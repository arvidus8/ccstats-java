/*
 * Cloud Clinic API
 * API for Cloud Clinic
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;

/**
 * AppointmentForUpdate
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-03-28T10:16:13.373+02:00")
public class AppointmentForUpdate {
  @SerializedName("alarm_ts")
  private OffsetDateTime alarmTs = null;

  @SerializedName("location_id")
  private Integer locationId = null;

  @SerializedName("notes")
  private String notes = null;

  @SerializedName("start_time")
  private OffsetDateTime startTime = null;

  @SerializedName("stop_time")
  private OffsetDateTime stopTime = null;

  @SerializedName("summary")
  private String summary = null;

  @SerializedName("user_participants")
  private List<String> userParticipants = null;

  public AppointmentForUpdate alarmTs(OffsetDateTime alarmTs) {
    this.alarmTs = alarmTs;
    return this;
  }

   /**
   * The alert time for the appointment
   * @return alarmTs
  **/
  @ApiModelProperty(value = "The alert time for the appointment")
  public OffsetDateTime getAlarmTs() {
    return alarmTs;
  }

  public void setAlarmTs(OffsetDateTime alarmTs) {
    this.alarmTs = alarmTs;
  }

  public AppointmentForUpdate locationId(Integer locationId) {
    this.locationId = locationId;
    return this;
  }

   /**
   * The Location ID for the appointment
   * @return locationId
  **/
  @ApiModelProperty(value = "The Location ID for the appointment")
  public Integer getLocationId() {
    return locationId;
  }

  public void setLocationId(Integer locationId) {
    this.locationId = locationId;
  }

  public AppointmentForUpdate notes(String notes) {
    this.notes = notes;
    return this;
  }

   /**
   * Appointment notes
   * @return notes
  **/
  @ApiModelProperty(value = "Appointment notes")
  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public AppointmentForUpdate startTime(OffsetDateTime startTime) {
    this.startTime = startTime;
    return this;
  }

   /**
   * Start-time for the appointment
   * @return startTime
  **/
  @ApiModelProperty(required = true, value = "Start-time for the appointment")
  public OffsetDateTime getStartTime() {
    return startTime;
  }

  public void setStartTime(OffsetDateTime startTime) {
    this.startTime = startTime;
  }

  public AppointmentForUpdate stopTime(OffsetDateTime stopTime) {
    this.stopTime = stopTime;
    return this;
  }

   /**
   * Stop-time for the appointment
   * @return stopTime
  **/
  @ApiModelProperty(required = true, value = "Stop-time for the appointment")
  public OffsetDateTime getStopTime() {
    return stopTime;
  }

  public void setStopTime(OffsetDateTime stopTime) {
    this.stopTime = stopTime;
  }

  public AppointmentForUpdate summary(String summary) {
    this.summary = summary;
    return this;
  }

   /**
   * Summary of the appointment
   * @return summary
  **/
  @ApiModelProperty(value = "Summary of the appointment")
  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public AppointmentForUpdate userParticipants(List<String> userParticipants) {
    this.userParticipants = userParticipants;
    return this;
  }

  public AppointmentForUpdate addUserParticipantsItem(String userParticipantsItem) {
    if (this.userParticipants == null) {
      this.userParticipants = new ArrayList<String>();
    }
    this.userParticipants.add(userParticipantsItem);
    return this;
  }

   /**
   * List of User IDs, the Users who shall participate in the appointment
   * @return userParticipants
  **/
  @ApiModelProperty(value = "List of User IDs, the Users who shall participate in the appointment")
  public List<String> getUserParticipants() {
    return userParticipants;
  }

  public void setUserParticipants(List<String> userParticipants) {
    this.userParticipants = userParticipants;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AppointmentForUpdate appointmentForUpdate = (AppointmentForUpdate) o;
    return Objects.equals(this.alarmTs, appointmentForUpdate.alarmTs) &&
        Objects.equals(this.locationId, appointmentForUpdate.locationId) &&
        Objects.equals(this.notes, appointmentForUpdate.notes) &&
        Objects.equals(this.startTime, appointmentForUpdate.startTime) &&
        Objects.equals(this.stopTime, appointmentForUpdate.stopTime) &&
        Objects.equals(this.summary, appointmentForUpdate.summary) &&
        Objects.equals(this.userParticipants, appointmentForUpdate.userParticipants);
  }

  @Override
  public int hashCode() {
    return Objects.hash(alarmTs, locationId, notes, startTime, stopTime, summary, userParticipants);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AppointmentForUpdate {\n");
    
    sb.append("    alarmTs: ").append(toIndentedString(alarmTs)).append("\n");
    sb.append("    locationId: ").append(toIndentedString(locationId)).append("\n");
    sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
    sb.append("    startTime: ").append(toIndentedString(startTime)).append("\n");
    sb.append("    stopTime: ").append(toIndentedString(stopTime)).append("\n");
    sb.append("    summary: ").append(toIndentedString(summary)).append("\n");
    sb.append("    userParticipants: ").append(toIndentedString(userParticipants)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}


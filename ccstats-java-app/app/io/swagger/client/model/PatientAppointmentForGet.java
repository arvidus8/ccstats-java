/*
 * Cloud Clinic API
 * API for Cloud Clinic
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.PatientAppointmentForPost;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;

/**
 * PatientAppointmentForGet
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-03-28T10:16:13.373+02:00")
public class PatientAppointmentForGet {
  @SerializedName("alarm_ts")
  private OffsetDateTime alarmTs = null;

  @SerializedName("clinic_id")
  private String clinicId = null;

  @SerializedName("created_by")
  private String createdBy = null;

  @SerializedName("location_id")
  private Integer locationId = null;

  @SerializedName("notes")
  private String notes = null;

  @SerializedName("start_time")
  private OffsetDateTime startTime = null;

  @SerializedName("stop_time")
  private OffsetDateTime stopTime = null;

  @SerializedName("summary")
  private String summary = null;

  @SerializedName("user_participants")
  private List<String> userParticipants = null;

  @SerializedName("created_ts")
  private OffsetDateTime createdTs = null;

  @SerializedName("id")
  private Integer id = null;

  @SerializedName("is_clinic_appointment")
  private Boolean isClinicAppointment = true;

  @SerializedName("last_modified_ts")
  private OffsetDateTime lastModifiedTs = null;

  public PatientAppointmentForGet alarmTs(OffsetDateTime alarmTs) {
    this.alarmTs = alarmTs;
    return this;
  }

   /**
   * The alert time for the appointment
   * @return alarmTs
  **/
  @ApiModelProperty(value = "The alert time for the appointment")
  public OffsetDateTime getAlarmTs() {
    return alarmTs;
  }

  public void setAlarmTs(OffsetDateTime alarmTs) {
    this.alarmTs = alarmTs;
  }

  public PatientAppointmentForGet clinicId(String clinicId) {
    this.clinicId = clinicId;
    return this;
  }

   /**
   * The Clinic Id for the appointment
   * @return clinicId
  **/
  @ApiModelProperty(required = true, value = "The Clinic Id for the appointment")
  public String getClinicId() {
    return clinicId;
  }

  public void setClinicId(String clinicId) {
    this.clinicId = clinicId;
  }

  public PatientAppointmentForGet createdBy(String createdBy) {
    this.createdBy = createdBy;
    return this;
  }

   /**
   * The User ID of the creator of the appointment
   * @return createdBy
  **/
  @ApiModelProperty(required = true, value = "The User ID of the creator of the appointment")
  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public PatientAppointmentForGet locationId(Integer locationId) {
    this.locationId = locationId;
    return this;
  }

   /**
   * The Location ID for the appointment
   * @return locationId
  **/
  @ApiModelProperty(value = "The Location ID for the appointment")
  public Integer getLocationId() {
    return locationId;
  }

  public void setLocationId(Integer locationId) {
    this.locationId = locationId;
  }

  public PatientAppointmentForGet notes(String notes) {
    this.notes = notes;
    return this;
  }

   /**
   * Appointment notes
   * @return notes
  **/
  @ApiModelProperty(value = "Appointment notes")
  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public PatientAppointmentForGet startTime(OffsetDateTime startTime) {
    this.startTime = startTime;
    return this;
  }

   /**
   * Start-time for the appointment
   * @return startTime
  **/
  @ApiModelProperty(required = true, value = "Start-time for the appointment")
  public OffsetDateTime getStartTime() {
    return startTime;
  }

  public void setStartTime(OffsetDateTime startTime) {
    this.startTime = startTime;
  }

  public PatientAppointmentForGet stopTime(OffsetDateTime stopTime) {
    this.stopTime = stopTime;
    return this;
  }

   /**
   * Stop-time for the appointment
   * @return stopTime
  **/
  @ApiModelProperty(required = true, value = "Stop-time for the appointment")
  public OffsetDateTime getStopTime() {
    return stopTime;
  }

  public void setStopTime(OffsetDateTime stopTime) {
    this.stopTime = stopTime;
  }

  public PatientAppointmentForGet summary(String summary) {
    this.summary = summary;
    return this;
  }

   /**
   * Summary of the appointment
   * @return summary
  **/
  @ApiModelProperty(value = "Summary of the appointment")
  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public PatientAppointmentForGet userParticipants(List<String> userParticipants) {
    this.userParticipants = userParticipants;
    return this;
  }

  public PatientAppointmentForGet addUserParticipantsItem(String userParticipantsItem) {
    if (this.userParticipants == null) {
      this.userParticipants = new ArrayList<String>();
    }
    this.userParticipants.add(userParticipantsItem);
    return this;
  }

   /**
   * List of User IDs, the Users who shall participate in the appointment
   * @return userParticipants
  **/
  @ApiModelProperty(value = "List of User IDs, the Users who shall participate in the appointment")
  public List<String> getUserParticipants() {
    return userParticipants;
  }

  public void setUserParticipants(List<String> userParticipants) {
    this.userParticipants = userParticipants;
  }

  public PatientAppointmentForGet createdTs(OffsetDateTime createdTs) {
    this.createdTs = createdTs;
    return this;
  }

   /**
   * The time an appointment was created
   * @return createdTs
  **/
  @ApiModelProperty(value = "The time an appointment was created")
  public OffsetDateTime getCreatedTs() {
    return createdTs;
  }

  public void setCreatedTs(OffsetDateTime createdTs) {
    this.createdTs = createdTs;
  }

  public PatientAppointmentForGet id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * The unique identifier of an appointment
   * @return id
  **/
  @ApiModelProperty(value = "The unique identifier of an appointment")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

   /**
   * Indicates whether this appointment is intended for patient or clinic staff
   * @return isClinicAppointment
  **/
  @ApiModelProperty(value = "Indicates whether this appointment is intended for patient or clinic staff")
  public Boolean isIsClinicAppointment() {
    return isClinicAppointment;
  }

  public PatientAppointmentForGet lastModifiedTs(OffsetDateTime lastModifiedTs) {
    this.lastModifiedTs = lastModifiedTs;
    return this;
  }

   /**
   * The time an appointment was last modified
   * @return lastModifiedTs
  **/
  @ApiModelProperty(value = "The time an appointment was last modified")
  public OffsetDateTime getLastModifiedTs() {
    return lastModifiedTs;
  }

  public void setLastModifiedTs(OffsetDateTime lastModifiedTs) {
    this.lastModifiedTs = lastModifiedTs;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PatientAppointmentForGet patientAppointmentForGet = (PatientAppointmentForGet) o;
    return Objects.equals(this.alarmTs, patientAppointmentForGet.alarmTs) &&
        Objects.equals(this.clinicId, patientAppointmentForGet.clinicId) &&
        Objects.equals(this.createdBy, patientAppointmentForGet.createdBy) &&
        Objects.equals(this.locationId, patientAppointmentForGet.locationId) &&
        Objects.equals(this.notes, patientAppointmentForGet.notes) &&
        Objects.equals(this.startTime, patientAppointmentForGet.startTime) &&
        Objects.equals(this.stopTime, patientAppointmentForGet.stopTime) &&
        Objects.equals(this.summary, patientAppointmentForGet.summary) &&
        Objects.equals(this.userParticipants, patientAppointmentForGet.userParticipants) &&
        Objects.equals(this.createdTs, patientAppointmentForGet.createdTs) &&
        Objects.equals(this.id, patientAppointmentForGet.id) &&
        Objects.equals(this.isClinicAppointment, patientAppointmentForGet.isClinicAppointment) &&
        Objects.equals(this.lastModifiedTs, patientAppointmentForGet.lastModifiedTs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(alarmTs, clinicId, createdBy, locationId, notes, startTime, stopTime, summary, userParticipants, createdTs, id, isClinicAppointment, lastModifiedTs);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PatientAppointmentForGet {\n");
    
    sb.append("    alarmTs: ").append(toIndentedString(alarmTs)).append("\n");
    sb.append("    clinicId: ").append(toIndentedString(clinicId)).append("\n");
    sb.append("    createdBy: ").append(toIndentedString(createdBy)).append("\n");
    sb.append("    locationId: ").append(toIndentedString(locationId)).append("\n");
    sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
    sb.append("    startTime: ").append(toIndentedString(startTime)).append("\n");
    sb.append("    stopTime: ").append(toIndentedString(stopTime)).append("\n");
    sb.append("    summary: ").append(toIndentedString(summary)).append("\n");
    sb.append("    userParticipants: ").append(toIndentedString(userParticipants)).append("\n");
    sb.append("    createdTs: ").append(toIndentedString(createdTs)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    isClinicAppointment: ").append(toIndentedString(isClinicAppointment)).append("\n");
    sb.append("    lastModifiedTs: ").append(toIndentedString(lastModifiedTs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}


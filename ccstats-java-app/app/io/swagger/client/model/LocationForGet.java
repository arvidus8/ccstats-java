/*
 * Cloud Clinic API
 * API for Cloud Clinic
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package io.swagger.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.client.model.LocationForPosting;
import java.io.IOException;

/**
 * LocationForGet
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-03-28T10:16:13.373+02:00")
public class LocationForGet {
  @SerializedName("building")
  private String building = null;

  @SerializedName("floor")
  private String floor = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("room")
  private String room = null;

  @SerializedName("clinic_id")
  private String clinicId = null;

  @SerializedName("id")
  private Integer id = null;

  public LocationForGet building(String building) {
    this.building = building;
    return this;
  }

   /**
   * Location building
   * @return building
  **/
  @ApiModelProperty(value = "Location building")
  public String getBuilding() {
    return building;
  }

  public void setBuilding(String building) {
    this.building = building;
  }

  public LocationForGet floor(String floor) {
    this.floor = floor;
    return this;
  }

   /**
   * Location floor
   * @return floor
  **/
  @ApiModelProperty(value = "Location floor")
  public String getFloor() {
    return floor;
  }

  public void setFloor(String floor) {
    this.floor = floor;
  }

  public LocationForGet name(String name) {
    this.name = name;
    return this;
  }

   /**
   * The name of the location
   * @return name
  **/
  @ApiModelProperty(required = true, value = "The name of the location")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public LocationForGet room(String room) {
    this.room = room;
    return this;
  }

   /**
   * Location room
   * @return room
  **/
  @ApiModelProperty(value = "Location room")
  public String getRoom() {
    return room;
  }

  public void setRoom(String room) {
    this.room = room;
  }

  public LocationForGet clinicId(String clinicId) {
    this.clinicId = clinicId;
    return this;
  }

   /**
   * The Clinic Id for the location
   * @return clinicId
  **/
  @ApiModelProperty(required = true, value = "The Clinic Id for the location")
  public String getClinicId() {
    return clinicId;
  }

  public void setClinicId(String clinicId) {
    this.clinicId = clinicId;
  }

  public LocationForGet id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * The unique identifier of a Location
   * @return id
  **/
  @ApiModelProperty(value = "The unique identifier of a Location")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LocationForGet locationForGet = (LocationForGet) o;
    return Objects.equals(this.building, locationForGet.building) &&
        Objects.equals(this.floor, locationForGet.floor) &&
        Objects.equals(this.name, locationForGet.name) &&
        Objects.equals(this.room, locationForGet.room) &&
        Objects.equals(this.clinicId, locationForGet.clinicId) &&
        Objects.equals(this.id, locationForGet.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(building, floor, name, room, clinicId, id);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LocationForGet {\n");
    
    sb.append("    building: ").append(toIndentedString(building)).append("\n");
    sb.append("    floor: ").append(toIndentedString(floor)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    room: ").append(toIndentedString(room)).append("\n");
    sb.append("    clinicId: ").append(toIndentedString(clinicId)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

